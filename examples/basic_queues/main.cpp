#include "satos/kernel.h"
#include "satos/thread.h"
#include "satos/message_queue.h"
#include "satext/units.h"

using namespace std::chrono_literals;

class consumer_thread;
using consumer_thread_base = satos::thread<consumer_thread>;

class consumer_thread : public consumer_thread_base {
    friend consumer_thread_base;

public:
    satos::message_queue<std::uint32_t, 8> queue;

private:
    [[noreturn]] void thread_function() {
        while (true) {
            if (auto msg = queue.pull(satos::kernel::clock::duration::max()); msg.has_value()) {
                do_stuff(msg.value());
            }
        }
    }

    void do_stuff(std::uint32_t) { /*...*/
    }
};

class producer_thread;
using producer_thread_base = satos::thread<producer_thread>;

class producer_thread : public producer_thread_base {
    friend producer_thread_base;

public:
    explicit producer_thread(consumer_thread& consumer) : _consumer(consumer) {}

private:
    [[noreturn]] void thread_function() {
        while (true) {
            std::uint32_t msg = generate_data();
            if (!_consumer.queue.push(msg)) {
                // sadness intensifies
            }
            satos::this_thread::sleep_for(100ms);
        }
    }

    consumer_thread& _consumer;
    std::uint32_t generate_data() {
        static std::uint32_t i;
        return i++;
    }
};

consumer_thread consumer;
producer_thread producer{consumer};

void interrupt_handler() {
    std::uint32_t data;
    static_cast<void>(consumer.queue.push(data));
}

int main() {
    satos::kernel::initialize(100_MHz, 100_Hz);
    consumer.start();
    producer.start();
    satos::kernel::start();
    while (true) {}
}
