#include "satos/mutex.h"
#include "FreeRTOS.h"
#include "semphr.h"

namespace satos {
class mutex::impl {
public:
    impl();
    impl(const impl&) = delete;
    impl& operator=(const impl&) = delete;
    impl(impl&&) noexcept = delete;
    impl& operator=(impl&&) noexcept = delete;
    ~impl() noexcept;

    void lock();

    bool try_lock();

    bool try_lock_for(std::uint32_t ticks);

    void unlock();

    [[nodiscard]] mutex_native_handle native_handle();

private:
    SemaphoreHandle_t mutex_handle_{nullptr};
    StaticSemaphore_t mutex_control_block_{};
};

mutex::impl::impl() {
    if (!detail::is_isr()) {
        mutex_handle_ = xSemaphoreCreateMutexStatic(&mutex_control_block_);
        if constexpr (configQUEUE_REGISTRY_SIZE > 0) {
            vQueueAddToRegistry(mutex_handle_, nullptr);
        }
    }
}

mutex::impl::~impl() noexcept {
    if (!detail::is_isr()) {
        if constexpr (configQUEUE_REGISTRY_SIZE > 0) {
            vQueueUnregisterQueue(mutex_handle_);
        }
        vSemaphoreDelete(mutex_handle_);
    }
}

void mutex::impl::lock() {
    if (!detail::is_isr()) {
        if (mutex_handle_ == nullptr) {
            kernel::error_handler(kernel::error::nullptr_dereference);
        } else {
            xSemaphoreTake(mutex_handle_, portMAX_DELAY);
        }
    }
}

bool mutex::impl::try_lock() {
    if (!detail::is_isr()) {
        if (mutex_handle_ == nullptr) {
            kernel::error_handler(kernel::error::nullptr_dereference);
            return false;
        }
        return xSemaphoreTake(mutex_handle_, 0) == pdTRUE;
    } else {
        return false;
    }
}

bool mutex::impl::try_lock_for(std::uint32_t ticks) {
    if (!detail::is_isr()) {
        if (mutex_handle_ == nullptr) {
            kernel::error_handler(kernel::error::nullptr_dereference);
            return false;
        }
        return xSemaphoreTake(mutex_handle_, ticks) == pdTRUE;
    } else {
        return false;
    }
}

void mutex::impl::unlock() {
    if (!detail::is_isr()) {
        if (mutex_handle_ == nullptr) {
            kernel::error_handler(kernel::error::nullptr_dereference);
        } else {
            xSemaphoreGive(mutex_handle_);
        }
    }
}

mutex_native_handle mutex::impl::native_handle() { return mutex_handle_; }

void mutex::lock() { impl_.lock(); }

bool mutex::try_lock() { return impl_.try_lock(); }

bool mutex::try_lock_for(satos::clock::duration timeout) {
    return impl_.try_lock_for(satos::clock::to_ticks(timeout));
}

bool mutex::try_lock_until(satos::clock::time_point sleep_time) {
    auto ticks = satos::clock::to_ticks(sleep_time);
    auto now = xTaskGetTickCount();
    return impl_.try_lock_for(ticks - now);
}

void mutex::unlock() { impl_.unlock(); }

mutex_native_handle mutex::native_handle() { return impl_.native_handle(); }

mutex::mutex() : impl_{*reinterpret_cast<impl*>(&impl_storage_)}, impl_storage_{} {
    static_assert(sizeof(impl) == sizeof(impl_storage_));
    static_assert(std::alignment_of<impl>::value == std::alignment_of<impl_storage>::value);
    new (&impl_storage_) impl;
}

mutex::~mutex() noexcept { reinterpret_cast<impl*>(&impl_storage_)->~impl(); }

class recursive_mutex::impl {
public:
    impl();
    impl(const impl&) = delete;
    impl& operator=(const impl&) = delete;
    impl(impl&&) noexcept = delete;
    impl& operator=(impl&&) noexcept = delete;
    ~impl() noexcept;

    void lock();

    bool try_lock();

    bool try_lock_for(std::uint32_t ticks);

    void unlock();

    [[nodiscard]] mutex_native_handle native_handle();

private:
    SemaphoreHandle_t mutex_handle_{nullptr};
    StaticSemaphore_t mutex_control_block_{};
};

recursive_mutex::impl::impl() {
    if (!detail::is_isr()) {
        mutex_handle_ = xSemaphoreCreateRecursiveMutexStatic(&mutex_control_block_);
        if constexpr (configQUEUE_REGISTRY_SIZE > 0) {
            vQueueAddToRegistry(mutex_handle_, nullptr);
        }
    }
}

recursive_mutex::impl::~impl() noexcept {
    if (!detail::is_isr()) {
        if constexpr (configQUEUE_REGISTRY_SIZE > 0) {
            vQueueUnregisterQueue(mutex_handle_);
        }
        vSemaphoreDelete(mutex_handle_);
    }
}

void recursive_mutex::impl::lock() {
    if (!detail::is_isr()) {
        if (mutex_handle_ == nullptr) {
            kernel::error_handler(kernel::error::nullptr_dereference);
        } else {
            xSemaphoreTakeRecursive(mutex_handle_, portMAX_DELAY);
        }
    }
}

bool recursive_mutex::impl::try_lock() {
    if (!detail::is_isr()) {
        if (mutex_handle_ == nullptr) {
            kernel::error_handler(kernel::error::nullptr_dereference);
            return false;
        }
        return xSemaphoreTakeRecursive(mutex_handle_, 0) == pdTRUE;
    } else {
        return false;
    }
}

bool recursive_mutex::impl::try_lock_for(std::uint32_t ticks) {
    if (!detail::is_isr()) {
        if (mutex_handle_ == nullptr) {
            kernel::error_handler(kernel::error::nullptr_dereference);
            return false;
        }
        return xSemaphoreTakeRecursive(mutex_handle_, ticks) == pdTRUE;
    } else {
        return false;
    }
}

void recursive_mutex::impl::unlock() {
    if (!detail::is_isr()) {
        if (mutex_handle_ == nullptr) {
            kernel::error_handler(kernel::error::nullptr_dereference);
        } else {
            xSemaphoreGiveRecursive(mutex_handle_);
        }
    }
}

mutex_native_handle recursive_mutex::impl::native_handle() { return mutex_handle_; }

recursive_mutex::recursive_mutex() :
    impl_{*reinterpret_cast<impl*>(&impl_storage_)}, impl_storage_{} {
    static_assert(sizeof(impl) == sizeof(impl_storage_));
    static_assert(std::alignment_of<impl>::value == std::alignment_of<impl_storage>::value);
    new (&impl_storage_) impl;
}

void recursive_mutex::lock() { impl_.lock(); }

void recursive_mutex::unlock() { impl_.unlock(); }

mutex_native_handle recursive_mutex::native_handle() { return impl_.native_handle(); }

recursive_mutex::~recursive_mutex() noexcept { reinterpret_cast<impl*>(&impl_storage_)->~impl(); }

bool recursive_mutex::try_lock() { return impl_.try_lock(); }

bool recursive_mutex::try_lock_for(satos::clock::duration timeout) {
    return impl_.try_lock_for(satos::clock::to_ticks(timeout));
}

bool recursive_mutex::try_lock_until(satos::clock::time_point sleep_time) {
    auto ticks = satos::clock::to_ticks(sleep_time);
    auto now = xTaskGetTickCount();
    return impl_.try_lock_for(ticks - now);
}

} // namespace satos
