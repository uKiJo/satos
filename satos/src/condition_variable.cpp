#include "satos/condition_variable.h"
#include "FreeRTOS.h"
#include "semphr.h"

namespace satos::detail {
void condition_variable_base::atomic_unlock_and_suspend(std::unique_lock<mutex>& lock,
                                                        std::uint32_t ticks_to_wait) {
    /* lock.unlock() and task suspend must be atomic */
    portDISABLE_INTERRUPTS();

    /* due to disabled interrupts this call only set owns property of unique_lock to false */
    lock.unlock();

    auto lock_control_block = reinterpret_cast<StaticSemaphore_t*>(lock.mutex()->native_handle());
    /* when giving mutex from interrupt
     * assert is made to make sure no mutex is given from interrupt, so change it type
     */
    void* mutex_handle = lock_control_block->pvDummy1[2];
    lock_control_block->pvDummy1[2] = nullptr;
    /* unlock mutex from isr so it wont enable interrupts */
    xSemaphoreGiveFromISR(lock.mutex()->native_handle(), nullptr);
    /* reset mutex type */
    lock_control_block->pvDummy1[2] = mutex_handle;

    /* suspend calling thread */

    ulTaskNotifyTake(pdTRUE, ticks_to_wait);
    /* no need to enable interrupts because they are enabled at the end of notify take call */
}

void condition_variable_base::notify_thread(thread_native_handle thread) {
    xTaskNotifyGive(static_cast<TaskHandle_t>(thread));
}

void condition_variable_base::notify_thread_from_isr(thread_native_handle thread,
                                                     std::int32_t* yield) {
    vTaskNotifyGiveFromISR(static_cast<TaskHandle_t>(thread), yield);
}

void condition_variable_base::yield_from_isr(std::int32_t yield) { portYIELD_FROM_ISR(yield); }
} // namespace satos::detail
