#include "satos/kernel.h"
#include "FreeRTOS.h"
#include "task.h"

std::uint32_t freertos_cpu_hz = 0;
std::uint32_t freertos_tick_rate_hz = 0;

namespace satos {
kernel::status kernel::initialize(std::uint32_t cpu_hz, std::uint32_t tick_rate_hz) {
    if (detail::is_isr()) {
        return status::error_isr;
    }
    if (get_instance().state_ == state::inactive) {
        // TODO: heap 5
        get_instance().state_ = state::ready;
        freertos_cpu_hz = cpu_hz;
        freertos_tick_rate_hz = tick_rate_hz;
        get_instance().tick_duration_ms_ = 1000 / freertos_tick_rate_hz;
        return status::ok;
    } else {
        return status::error;
    }
}

kernel::status kernel::start() {
    if (detail::is_isr()) {
        return status::error_isr;
    }
    if (get_instance().state_ == state::ready) {
        get_instance().state_ = state::running;
        vTaskStartScheduler();
        return status::ok;
    } else {
        return status::error;
    }
}

kernel::state kernel::get_state() {
    switch (xTaskGetSchedulerState()) {
    case taskSCHEDULER_RUNNING:
        return state::running;
    case taskSCHEDULER_SUSPENDED:
        return state::locked;
    case taskSCHEDULER_NOT_STARTED:
    default:
        if (get_instance().state_ == state::ready) {
            return state::ready;
        } else {
            return state::inactive;
        }
    }
}

kernel::status kernel::lock() {
    if (detail::is_isr()) {
        return status::error_isr;
    }
    switch (xTaskGetSchedulerState()) {
    case taskSCHEDULER_RUNNING:
        vTaskSuspendAll();
        return status::ok;
    case taskSCHEDULER_SUSPENDED:
        return status::ok;
    case taskSCHEDULER_NOT_STARTED:
    default:
        return status::error;
    }
}

kernel::status kernel::unlock() {
    if (detail::is_isr()) {
        return status::error_isr;
    }
    switch (xTaskGetSchedulerState()) {
    case taskSCHEDULER_SUSPENDED:
        if (xTaskResumeAll() != pdTRUE) {
            if (xTaskGetSchedulerState() == taskSCHEDULER_SUSPENDED) {
                return status::error;
            }
        }
        return status::ok;
    case taskSCHEDULER_RUNNING:
        return status::ok;
    case taskSCHEDULER_NOT_STARTED:
    default:
        return status::error;
    }
}

std::uint32_t kernel::get_tick_count() {
    if (detail::is_isr()) {
        return xTaskGetTickCountFromISR();
    } else {
        return xTaskGetTickCount();
    }
}

std::uint32_t kernel::get_tick_freq() { return freertos_tick_rate_hz; }

void kernel::stop() { vTaskEndScheduler(); }

void kernel::enter_critical() { portENTER_CRITICAL(); }

void kernel::exit_critical() { portEXIT_CRITICAL(); }

void kernel::idle_handler() {
    if (auto handler = get_instance().idle_handler_) {
        handler();
    }
}

void kernel::set_idle_handler(const satext::inplace_function<void()> handler) {
    get_instance().idle_handler_ = handler;
}

void kernel::set_error_handler(const satext::inplace_function<void(kernel::error)> handler) {
    get_instance().error_handler_ = handler;
}

void kernel::isr_push() {
    auto& instance = get_instance();
    if (instance.isr_yield_stack_ptr_ == instance.isr_yield_stack_.begin()) {
        instance.error_handler_(error::out_of_memory);
    }
    if (instance.isr_yield_stack_ptr_ != instance.isr_yield_stack_.end()) {
        *instance.isr_yield_stack_ptr_ = static_cast<std::int8_t>(instance.current_isr_yield_);
    }
    instance.current_isr_yield_ = pdFALSE;
    instance.isr_yield_stack_ptr_--;
}

std::int32_t kernel::isr_pop() {
    auto& instance = get_instance();
    if (instance.isr_yield_stack_ptr_ != instance.isr_yield_stack_.end()) {
        auto result = instance.current_isr_yield_;
        instance.isr_yield_stack_ptr_++;

        if (instance.isr_yield_stack_ptr_ != instance.isr_yield_stack_.end()) {
            instance.current_isr_yield_ = static_cast<std::uint8_t>(*instance.isr_yield_stack_ptr_);
            *instance.isr_yield_stack_ptr_ = pdFALSE;
        } else {
            instance.current_isr_yield_ = pdFALSE;
        }

        return result;
    } else {
        return pdFALSE;
    }
}

std::int32_t* kernel::get_isr_yield() { return &get_instance().current_isr_yield_; }

void kernel::expects(bool x) { configASSERT(x); }

std::uint32_t kernel::get_tick_duration_ms() { return get_instance().tick_duration_ms_; }

void kernel::error_handler(kernel::error error) {
    if (auto handler = get_instance().error_handler_) {
        handler(error);
    }
}

kernel& kernel::get_instance() {
    static kernel k;
    return k;
}

} // namespace satos
