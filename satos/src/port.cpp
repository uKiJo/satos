#include "satos/port.h"
#include "satos/kernel.h"

namespace satos::detail {

__attribute__((always_inline)) inline std::uint32_t get_primask() {
    std::uint32_t result;

    asm volatile("MRS %0, primask" : "=r"(result)::"memory");
    return result;
}

__attribute__((always_inline)) inline std::uint32_t get_basepri() {
    std::uint32_t result;

    asm volatile("MRS %0, basepri" : "=r"(result));
    return result;
}

__attribute__((always_inline)) inline std::uint32_t get_ipsr() {
    std::uint32_t result;

    asm volatile("MRS %0, ipsr" : "=r"(result));
    return result;
}

bool is_isr_masked() {
    if constexpr (arm_arch_7m || arm_arch_7em || arm_arch_8m_main) {
        return (get_primask() != 0U) ||
               ((kernel::get_state() == kernel::state::running) && (get_basepri() != 0U));
    } else if constexpr (arm_arch_6m) {
        return (get_primask() != 0U) || (kernel::get_state() == kernel::state::running);
    } else {
        return (get_primask() != 0U);
    }
}

bool is_isr_mode() { return (get_ipsr() != 0U); }

bool is_isr() { return is_isr_mode() || is_isr_masked(); }
} // namespace satos::detail
