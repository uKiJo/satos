#include "satos/semaphore.h"
#include "FreeRTOS.h"
#include "semphr.h"

namespace satos {
class binary_semaphore::impl {
public:
    impl();
    impl(const impl&) = delete;
    impl& operator=(const impl&) = delete;
    impl(impl&&) noexcept = delete;
    impl& operator=(impl&&) noexcept = delete;
    ~impl() noexcept;

    void release();

    void acquire();

    [[nodiscard]] bool try_acquire();

    [[nodiscard]] bool try_acquire_for(std::uint32_t ticks);

private:
    SemaphoreHandle_t semaphore_handle_{nullptr};
    StaticSemaphore_t semaphore_control_block_{};
};

binary_semaphore::impl::impl() {
    if (!detail::is_isr()) {
        semaphore_handle_ = xSemaphoreCreateBinaryStatic(&semaphore_control_block_);
        if constexpr (configQUEUE_REGISTRY_SIZE > 0) {
            vQueueAddToRegistry(semaphore_handle_, nullptr);
        }
    }
}

binary_semaphore::impl::~impl() noexcept {
    if (!detail::is_isr()) {
        if constexpr (configQUEUE_REGISTRY_SIZE > 0) {
            vQueueUnregisterQueue(semaphore_handle_);
        }
        vSemaphoreDelete(semaphore_handle_);
    }
}

void binary_semaphore::impl::release() {
    if (semaphore_handle_ != nullptr) {
        if (detail::is_isr()) {
            xSemaphoreGiveFromISR(semaphore_handle_, kernel::get_isr_yield());
        } else {
            xSemaphoreGive(semaphore_handle_);
        }
    } else {
        kernel::error_handler(kernel::error::nullptr_dereference);
    }
}

void binary_semaphore::impl::acquire() {
    if (!detail::is_isr()) {
        if (semaphore_handle_ == nullptr) {
            kernel::error_handler(kernel::error::nullptr_dereference);
            return;
        }
        xSemaphoreTake(semaphore_handle_, portMAX_DELAY);
    }
}

bool binary_semaphore::impl::try_acquire() {
    if (semaphore_handle_ == nullptr) {
        kernel::error_handler(kernel::error::nullptr_dereference);
        return false;
    }
    if (detail::is_isr()) {
        return xSemaphoreTakeFromISR(semaphore_handle_, kernel::get_isr_yield()) == pdTRUE;
    } else {
        return xSemaphoreTake(semaphore_handle_, 0) == pdTRUE;
    }
}

bool binary_semaphore::impl::try_acquire_for(const std::uint32_t ticks) {
    if (!detail::is_isr()) {
        if (semaphore_handle_ == nullptr) {
            kernel::error_handler(kernel::error::nullptr_dereference);
            return false;
        }
        return xSemaphoreTake(semaphore_handle_, ticks) == pdTRUE;
    } else {
        return false;
    }
}

binary_semaphore::binary_semaphore(std::uint32_t counter) :
    impl_{*reinterpret_cast<impl*>(&impl_storage_)}, impl_storage_{} {
    static_assert(sizeof(impl) == sizeof(impl_storage_));
    static_assert(std::alignment_of<impl>::value == std::alignment_of<impl_storage>::value);
    new (&impl_storage_) impl;
    if (counter != 0) {
        impl_.release();
    }
}

binary_semaphore::~binary_semaphore() noexcept { reinterpret_cast<impl*>(&impl_storage_)->~impl(); }

void binary_semaphore::release() { impl_.release(); }

void binary_semaphore::acquire() { impl_.acquire(); }

bool binary_semaphore::try_acquire() { return impl_.try_acquire(); }

bool binary_semaphore::try_acquire_for(satos::clock::duration rel_time) {
    return impl_.try_acquire_for(satos::clock::to_ticks(rel_time));
}
bool binary_semaphore::try_acquire_until(satos::clock::time_point abs_time) {
    auto ticks = satos::clock::to_ticks(abs_time);
    auto now = satos::kernel::get_tick_count();
    return impl_.try_acquire_for(ticks - now);
}
} // namespace satos
