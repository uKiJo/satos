#include "satos/event.h"
#include "FreeRTOS.h"
#include "task.h"

namespace satos {

void event::wait() {
    {
        critical_section c;
        kernel::expects(thread_ == nullptr);
        thread_ = this_thread::get_native_handle();
    }
    wait(portMAX_DELAY);
    thread_ = nullptr;
}

void event::notify() {
    auto thread = reinterpret_cast<TaskHandle_t>(thread_);
    if (thread == nullptr) {
        return;
    }
    if (detail::is_isr()) {
        xTaskNotifyFromISR(thread, 0, eNoAction, kernel::get_isr_yield());
    } else {
        xTaskNotify(thread, 0, eNoAction);
    }
}

bool event::wait(std::uint32_t ticks) { return xTaskNotifyWait(0, 0, nullptr, ticks); }

bool event::wait_for(clock::duration timeout) {
    {
        critical_section c;
        kernel::expects(thread_ == nullptr);
        thread_ = this_thread::get_native_handle();
    }
    auto result = wait(satos::clock::to_ticks(timeout));
    thread_ = nullptr;
    return result;
}

bool event::wait_until(clock::time_point sleep_time) {
    {
        critical_section c;
        kernel::expects(thread_ == nullptr);
        thread_ = this_thread::get_native_handle();
    }
    auto now = clock::now();
    auto result = wait(clock::to_ticks(sleep_time - now));
    thread_ = nullptr;
    return result;
}

} // namespace satos
