#pragma once

#include "satos/kernel.h"
#include "satos/clock.h"
#include "satos/concept/semaphore.h"

namespace satos {
class binary_semaphore {
public:
    explicit binary_semaphore(std::uint32_t counter = 0);
    binary_semaphore(const binary_semaphore&) = delete;
    binary_semaphore& operator=(const binary_semaphore&) = delete;
    binary_semaphore(binary_semaphore&&) noexcept = delete;
    binary_semaphore& operator=(binary_semaphore&&) noexcept = delete;

    ~binary_semaphore() noexcept;

    void release();

    void acquire();

    [[nodiscard]] bool try_acquire();

    [[nodiscard]] bool try_acquire_for(clock::duration rel_time);

    [[nodiscard]] bool try_acquire_until(clock::time_point abs_time);

private:
    class impl;
    impl& impl_;

#ifndef DEBUG
    static constexpr std::size_t size_of_impl = 76;
#else
    static constexpr std::size_t size_of_impl = 84;
#endif
    using impl_storage = std::aligned_storage<size_of_impl, 4>::type;
    impl_storage impl_storage_;
};

static_assert(semaphore_concept<binary_semaphore>);

} // namespace satos
