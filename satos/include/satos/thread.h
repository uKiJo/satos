#pragma once

#include "satos/kernel.h"
#include "satos/clock.h"
#include "satext/basic_fixed_string.h"
#include <concepts>

namespace satos {
using thread_native_handle = void*;

enum class thread_priority : std::uint32_t {
    none = 0,                                          ///< No priority (not initialized).
    idle = 1,                                          ///< Reserved for Idle thread.
    low_0 = 8,                                         ///< Priority: low
    low_1 = 8 + 1,                                     ///< Priority: low + 1
    low_2 = 8 + 2,                                     ///< Priority: low + 2
    low_3 = 8 + 3,                                     ///< Priority: low + 3
    low_4 = 8 + 4,                                     ///< Priority: low + 4
    low_5 = 8 + 5,                                     ///< Priority: low + 5
    low_6 = 8 + 6,                                     ///< Priority: low + 6
    low_7 = 8 + 7,                                     ///< Priority: low + 7
    below_normal_0 = 16,                               ///< Priority: below normal
    below_normal_1 = 16 + 1,                           ///< Priority: below normal + 1
    below_normal_2 = 16 + 2,                           ///< Priority: below normal + 2
    below_normal_3 = 16 + 3,                           ///< Priority: below normal + 3
    below_normal_4 = 16 + 4,                           ///< Priority: below normal + 4
    below_normal_5 = 16 + 5,                           ///< Priority: below normal + 5
    below_normal_6 = 16 + 6,                           ///< Priority: below normal + 6
    below_normal_7 = 16 + 7,                           ///< Priority: below normal + 7
    normal_0 = 24,                                     ///< Priority: normal
    normal_1 = 24 + 1,                                 ///< Priority: normal + 1
    normal_2 = 24 + 2,                                 ///< Priority: normal + 2
    normal_3 = 24 + 3,                                 ///< Priority: normal + 3
    normal_4 = 24 + 4,                                 ///< Priority: normal + 4
    normal_5 = 24 + 5,                                 ///< Priority: normal + 5
    normal_6 = 24 + 6,                                 ///< Priority: normal + 6
    normal_7 = 24 + 7,                                 ///< Priority: normal + 7
    above_normal_0 = 32,                               ///< Priority: above normal
    above_normal_1 = 32 + 1,                           ///< Priority: above normal + 1
    above_normal_2 = 32 + 2,                           ///< Priority: above normal + 2
    above_normal_3 = 32 + 3,                           ///< Priority: above normal + 3
    above_normal_4 = 32 + 4,                           ///< Priority: above normal + 4
    above_normal_5 = 32 + 5,                           ///< Priority: above normal + 5
    above_normal_6 = 32 + 6,                           ///< Priority: above normal + 6
    above_normal_7 = 32 + 7,                           ///< Priority: above normal + 7
    high_0 = 40,                                       ///< Priority: high
    high_1 = 40 + 1,                                   ///< Priority: high + 1
    high_2 = 40 + 2,                                   ///< Priority: high + 2
    high_3 = 40 + 3,                                   ///< Priority: high + 3
    high_4 = 40 + 4,                                   ///< Priority: high + 4
    high_5 = 40 + 5,                                   ///< Priority: high + 5
    high_6 = 40 + 6,                                   ///< Priority: high + 6
    high_7 = 40 + 7,                                   ///< Priority: high + 7
    realtime_0 = 48,                                   ///< Priority: realtime
    realtime_1 = 48 + 1,                               ///< Priority: realtime + 1
    realtime_2 = 48 + 2,                               ///< Priority: realtime + 2
    realtime_3 = 48 + 3,                               ///< Priority: realtime + 3
    realtime_4 = 48 + 4,                               ///< Priority: realtime + 4
    realtime_5 = 48 + 5,                               ///< Priority: realtime + 5
    realtime_6 = 48 + 6,                               ///< Priority: realtime + 6
    realtime_7 = 48 + 7,                               ///< Priority: realtime + 7
    isr = 56,                                          ///< Reserved for ISR deferred thread.
    error = std::numeric_limits<std::uint32_t>::max(), //< System cannot determine priority
                                                       //  or illegal priority.
};

enum class thread_state : std::int32_t {
    inactive = 0,
    ready = 1,
    running = 2,
    blocked = 3,
    suspended = 4,
    terminated = 5,
    error = -1,
};

namespace detail {
template<typename T>
concept callable_thread = requires(T t) {
                              { t.thread_function() } -> std::same_as<void>;
                          };

class thread_base {
public:
    thread_base(const thread_base&) = delete;
    thread_base& operator=(const thread_base&) = delete;
    thread_base(thread_base&&) noexcept = delete;
    thread_base& operator=(thread_base&&) noexcept = delete;

    void stop();

    kernel::status suspend();

    kernel::status resume();

    [[nodiscard]] thread_state get_state() const;

    [[nodiscard]] thread_priority get_priority() const;

    [[nodiscard]] thread_native_handle native_handle();

protected:
    using thread_funtion_ptr = void (*)(void*);

    thread_base();
    ~thread_base() noexcept;

    void start(thread_funtion_ptr thread_function, std::uint32_t* stack, size_t stack_size,
               thread_priority priority, const char* name);

private:
    class impl;
    impl& impl_;

    static constexpr std::size_t reent_size = 240;
#ifndef DEBUG
    static constexpr std::size_t size_of_impl = 88 + reent_size * SATOS_DYNAMIC_ALLOCATION;
#else
    static constexpr std::size_t size_of_impl = 100 + reent_size * SATOS_DYNAMIC_ALLOCATION;
#endif
    using impl_storage = std::aligned_storage<size_of_impl, 4>::type;
    impl_storage impl_storage_;
};
} // namespace detail

namespace this_thread {

void yield();

kernel::status suspend();

kernel::status resume();

void stop();

void sleep_for(clock::duration sleep_duration);

void sleep_until(clock::time_point sleep_time);

thread_native_handle get_native_handle();

} // namespace this_thread

template<class T, thread_priority priority = thread_priority::normal_0, size_t stack_size = 1024,
         satext::basic_fixed_string name = "">
class thread : public detail::thread_base {
    static_assert(stack_size != 0, "Stack size cannot be zero");

public:
    thread(const thread&) = delete;
    thread& operator=(const thread&) = delete;
    thread(thread&&) noexcept = delete;
    thread& operator=(thread&&) noexcept = delete;

    void start() {
        static_assert(detail::callable_thread<T>);
        thread_base::start(&thread::thread_function, stack_.data(), stack_.size(), priority, name);
    }

protected:
    thread() : thread_base() {}
    ~thread() noexcept = default;

    std::array<std::uint32_t, stack_size / sizeof(std::uint32_t)> stack_{};

    static void thread_function(void* args) {
        auto derived = static_cast<T*>(args);
        if (derived == nullptr) {
            kernel::error_handler(kernel::error::nullptr_dereference);
        } else {
            derived->thread_function();
        }
        this_thread::stop();
    }
};

} // namespace satos
