#pragma once

#include "satos/kernel.h"
#include "satos/clock.h"
#include <optional>

namespace satos {
namespace detail {
class message_queue_base {
public:
    message_queue_base(const message_queue_base&) = delete;
    message_queue_base& operator=(const message_queue_base&) = delete;
    message_queue_base(message_queue_base&&) noexcept = delete;
    message_queue_base& operator=(message_queue_base&&) noexcept = delete;

    [[nodiscard]] size_t size() const;

    void clear();

protected:
    message_queue_base(std::uint8_t* data, size_t queue_size, size_t item_size);
    ~message_queue_base() noexcept;

    [[nodiscard]] bool push(const void* in, std::uint32_t timeout);

    [[nodiscard]] bool pull(void* out, std::uint32_t timeout);

private:
    class impl;
    impl& impl_;

#ifndef DEBUG
    static constexpr std::size_t size_of_impl = 76;
#else
    static constexpr std::size_t size_of_impl = 84;
#endif
    using impl_storage = std::aligned_storage<size_of_impl, 4>::type;
    impl_storage impl_storage_;
};
} // namespace detail

template<class T, size_t queue_size>
class message_queue : public detail::message_queue_base {
    static_assert(queue_size != 0, "Queue size cannot be zero.");
    static_assert(std::is_trivially_copyable<T>::value,
                  "Queue message type must be trivially copyable");

public:
    message_queue() : message_queue_base(buffer_.data(), queue_size, sizeof(T)) {}
    message_queue(const message_queue&) = delete;
    message_queue& operator=(const message_queue&) = delete;
    message_queue(message_queue&&) noexcept = delete;
    message_queue& operator=(message_queue&&) noexcept = delete;
    ~message_queue() noexcept = default;

    [[nodiscard]] bool push(const T& in, clock::duration timeout = clock::duration ::zero()) {
        return message_queue_base::push(reinterpret_cast<const void*>(&in),
                                        clock::to_ticks(timeout));
    }

    [[nodiscard]] std::optional<T> pull(clock::duration timeout = clock::duration ::zero()) {
        T msg;
        if (message_queue_base::pull(reinterpret_cast<void*>(&msg), clock::to_ticks(timeout))) {
            return std::optional(std::move(msg));
        } else {
            return std::nullopt;
        }
    }

    [[nodiscard]] size_t capacity() const { return queue_size; }

    [[nodiscard]] bool empty() const { return size() == 0; }

private:
    std::array<std::uint8_t, queue_size * sizeof(T)> buffer_{};
};
} // namespace satos
