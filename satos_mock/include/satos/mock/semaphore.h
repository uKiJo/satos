#pragma once

#include "gmock/gmock.h"
#include "satos/semaphore.h"
#include "satos/kernel.h"
#include <vector>

namespace satos::mock {
struct semaphore {
    semaphore() { semaphores.push_back(this); }

    MOCK_METHOD(void, release, ());
    MOCK_METHOD(void, acquire, ());
    MOCK_METHOD(bool, try_acquire, ());
    MOCK_METHOD(bool, try_acquire_for, (const satos::kernel::clock::duration&));
    MOCK_METHOD(bool, try_acquire_until, (const satos::kernel::clock::time_point&));

    inline static std::vector<semaphore*> semaphores{};
};

static_assert(satos::semaphore_concept<satos::mock::semaphore>);
} // namespace satos::mock
