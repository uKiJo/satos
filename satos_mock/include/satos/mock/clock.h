#pragma once
#include "gmock/gmock.h"
#include "satos/clock.h"

namespace satos::mock {
class clock {
public:
    clock(const clock&) = delete;
    clock& operator=(const clock&) = delete;
    clock(clock&&) noexcept = delete;
    clock& operator=(clock&&) noexcept = delete;
    ~clock() noexcept = default;

    MOCK_METHOD(std::uint32_t, tick_duration_ms, ());
    MOCK_METHOD(satos::clock::time_point, now, ());
    MOCK_METHOD(satos::clock::time_point, from_now, (satos::clock::duration));
    MOCK_METHOD(std::uint32_t, to_ticks_from_duration, (satos::clock::duration));
    MOCK_METHOD(std::uint32_t, to_ticks_from_time_point, (satos::clock::time_point));

    static clock& instance();

    static void reset();

private:
    clock() = default;
};
} // namespace satos::mock
