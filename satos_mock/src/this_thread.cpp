#include "satos/mock/this_thread.h"
#include "satos/thread.h"

namespace satos {
namespace mock {
this_thread& this_thread::instance() {
    static this_thread t;
    return t;
}
void this_thread::reset() { ::testing::Mock::VerifyAndClear(&instance()); }
} // namespace mock

namespace this_thread {

void yield() { mock::this_thread::instance().yield(); }

kernel::status suspend() { return mock::this_thread::instance().suspend(); }

kernel::status resume() { return mock::this_thread::instance().resume(); }

void stop() { mock::this_thread::instance().stop(); }

void sleep_for(clock::duration duration) { mock::this_thread::instance().sleep_for(duration); }

void sleep_until(clock::time_point time) { mock::this_thread::instance().sleep_until(time); }

thread_native_handle get_native_handle() {
    return mock::this_thread::instance().get_native_handle();
}

} // namespace this_thread

} // namespace satos
