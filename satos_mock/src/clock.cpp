#include "satos/mock/clock.h"

namespace satos {
namespace mock {
clock& clock::instance() {
    static clock c;
    return c;
}
void clock::reset() { ::testing::Mock::VerifyAndClear(&instance()); }
} // namespace mock

std::uint32_t clock::tick_duration_ms() { return mock::clock::instance().tick_duration_ms(); }

clock::time_point clock::now() noexcept { return mock::clock::instance().now(); }

clock::time_point clock::from_now(clock::duration duration) noexcept {
    return mock::clock::instance().from_now(duration);
}

std::uint32_t clock::to_ticks(clock::duration duration) {
    return mock::clock::instance().to_ticks_from_duration(duration);
}

std::uint32_t clock::to_ticks(clock::time_point time) {
    return mock::clock::instance().to_ticks_from_time_point(time);
}

} // namespace satos
