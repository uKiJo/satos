set(TARGET satos_mock)

add_library(${TARGET} STATIC)
add_library(satos::mock ALIAS ${TARGET})

target_include_directories(${TARGET}
    PUBLIC
    include
    )

target_sources(${TARGET}
    PRIVATE
    src/clock.cpp
    src/kernel.cpp
    src/this_thread.cpp
    )


target_link_libraries(${TARGET}
    PUBLIC
    satos::api
    gmock
    )
