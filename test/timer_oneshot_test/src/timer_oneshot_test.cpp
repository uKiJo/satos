#include "satos/kernel.h"
#include "satos/thread.h"
#include "satos/timer.h"
#include "satos/tester.h"
#include "satext/units.h"

using namespace satos;
using namespace satos_tester;
using namespace satos::chrono_literals;

template<typename T>
class test_thread;

template<typename T>
using test_thread_base = thread<test_thread<T>, thread_priority::normal_0, 2048>;

template<typename T>
class test_thread : public test_thread_base<T> {
    friend test_thread_base<T>;

public:
    explicit test_thread(std::uint32_t& counter, T& timer) : counter_(counter), timer_(timer) {}

private:
    void thread_function() {
        { // test if timer is running, and callback wasnt called before timeout
            this_thread::sleep_for(40_ms);

            assert_that(counter_, is_eq(0));
            assert_that(timer_.is_running(), is_eq(true));
        }

        { // test if timer is stopped, and callback was called
            this_thread::sleep_for(20_ms);

            assert_that(counter_, is_eq(1));
            assert_that(timer_.is_running(), is_eq(false));
        }

        { // test if no more callbacks were called
            this_thread::sleep_for(200_ms);

            assert_that(counter_, is_eq(1));
            assert_that(timer_.is_running(), is_eq(false));
        }

        { // run timer with changed timeout,
            timer_.start(100_ms);
        }

        { // test if timer is running, and callback isnt called before new timeout
            this_thread::sleep_for(90_ms);

            assert_that(counter_, is_eq(1));
            assert_that(timer_.is_running(), is_eq(true));
        }

        { // test if timer is stopped, and callback was called
            this_thread::sleep_for(20_ms);

            assert_that(counter_, is_eq(2));
            assert_that(timer_.is_running(), is_eq(false));
        }

        end_test();
    }

    std::uint32_t& counter_;
    T& timer_;
};

int main(int argv, char** argc) {
    start_test("timer_oneshot_test", argv, argc);

    static std::uint32_t counter{};
    static timer t(50_ms, false);
    static test_thread test(counter, t);
    t.register_callback([]() {
        auto ticks = kernel::get_tick_count();
        static_cast<void>(ticks);
        counter++;
    });

    kernel::initialize(50_MHz, 100_Hz);

    t.start();
    test.start();

    kernel::start();
    return 0;
}
