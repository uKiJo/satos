#include "satos/kernel.h"
#include "satos/thread.h"
#include "satos/tester.h"
#include "satos/formatters/formatters.h"
#include "satext/units.h"

using namespace satos;
using namespace satos_tester;
using namespace satos::chrono_literals;
using time_point = satos::clock::time_point;

class stop_thread;
using stop_thread_base = thread<stop_thread, thread_priority::high_0>;
class stop_thread : public stop_thread_base {
    friend stop_thread_base;

private:
    void thread_function() {
        { // 1. test this_thread::suspend and resume from another thread
            this_thread::suspend();
            assert_that(clock::now(), is_eq(time_point(20_ms)));
        }

        { // 2. test this_thread::stop()
            this_thread::stop();
        }
    }
};

class test_thread;
using test_thread_base = thread<test_thread>;
class test_thread : public test_thread_base {
    friend test_thread_base;

public:
    explicit test_thread(stop_thread& thread) : thread_(thread) {}

private:
    void thread_function() {
        { // 1. test if another thread is block and releasing it
            assert_that(thread_.get_state(), is_eq(thread_state::suspended));
            this_thread::sleep_for(20_ms);
            thread_.resume();
        }

        this_thread::sleep_for(20_ms);

        { // 2. test this_thread::stop()
            assert_that(thread_.get_state(), is_eq(thread_state::terminated));
        }

        end_test();
    }

    stop_thread& thread_;
};

int main(int argv, char** argc) {
    start_test("this_thread_suspend_stop_test", argv, argc);
    static stop_thread s;
    static test_thread t(s);

    kernel::initialize(50_MHz, 100_Hz);
    t.start();
    s.start();
    kernel::start();
    return 0;
}
