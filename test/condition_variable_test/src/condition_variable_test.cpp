#include "satos/kernel.h"
#include "satos/thread.h"
#include "satos/mutex.h"
#include "satos/condition_variable.h"
#include "satos/tester.h"
#include "satos/formatters/formatters.h"
#include "satext/units.h"

using namespace satos;
using namespace satos_tester;
using namespace satos::chrono_literals;
using time_point = satos::clock::time_point;

template<typename T>
class high_prio_thread;
template<typename T>
using high_prio_thread_base = thread<high_prio_thread<T>, thread_priority::high_0, 2048>;
template<typename T>
class high_prio_thread : public high_prio_thread_base<T> {
    friend high_prio_thread_base<T>;

public:
    high_prio_thread(mutex& mtx, T& cv, bool& flag) : mtx_(mtx), cv_(cv), flag_(flag) {}

private:
    void thread_function() {
        { // 1. wait test
            std::unique_lock lck(mtx_);
            cv_.wait(lck);
            assert_that(clock::now(), is_eq(time_point(50_ms)));
        }
        { // 2. wait with predicate test
            std::unique_lock lck(mtx_);
            cv_.wait(lck, [this]() { return flag_; });
            assert_that(clock::now(), is_eq(time_point(100_ms)));
            flag_ = false;
        }

        { // 3. wait_for timeout test
            std::unique_lock lck(mtx_);
            auto result = cv_.wait_for(lck, 20_ms);
            assert_that(result, is_eq(cv_status::timeout));
            assert_that(clock::now(), is_eq(time_point(120_ms)));

            // 4. wait_for no timeout test
            result = cv_.wait_for(lck, 50_ms);
            assert_that(result, is_eq(cv_status::no_timeout));
            assert_that(clock::now(), is_eq(time_point(150_ms)));
        }

        { // 5. wait_for with predicate timeout with false predicate test
            std::unique_lock lck(mtx_);
            auto result = cv_.wait_for(lck, 20_ms, [this]() { return flag_; });
            assert_that(result, is_eq(false));
            assert_that(clock::now(), is_eq(time_point(170_ms)));

            // 6. wait_for with predicate timeout with true predicate test
            result = cv_.wait_for(lck, 20_ms, [this]() { return flag_; });
            assert_that(result, is_eq(true));
            assert_that(clock::now(), is_eq(time_point(190_ms)));
            flag_ = false;
        }

        { // 7. wait_for with predicate no timeout
            std::unique_lock lck(mtx_);
            auto result = cv_.wait_for(lck, 100_ms, [this]() { return flag_; });
            assert_that(result, is_eq(true));
            assert_that(clock::now(), is_eq(time_point(250_ms)));
            flag_ = false;
        }

        { // 8. wait_for with predicate timeout with false predicate test
            std::unique_lock lck(mtx_);
            auto now = clock::now();
            auto result = cv_.wait_until(lck, now + 20_ms, [this]() { return flag_; });
            assert_that(result, is_eq(false));
            assert_that(clock::now(), is_eq(time_point(270_ms)));

            // 9. wait_for with predicate timeout with true predicate test
            now = clock::now();
            result = cv_.wait_until(lck, now + 20_ms, [this]() { return flag_; });
            assert_that(result, is_eq(true));
            assert_that(clock::now(), is_eq(time_point(290_ms)));
            flag_ = false;
        }

        { // 10. wait_for with predicate no timeout
            std::unique_lock lck(mtx_);
            auto now = clock::now();
            auto result = cv_.wait_until(lck, now + 100_ms, [this]() { return flag_; });
            assert_that(result, is_eq(true));
            assert_that(clock::now(), is_eq(time_point(350_ms)));
        }

        { // 11. notify_all test
            std::unique_lock lck(mtx_);
            cv_.wait(lck);
            assert_that(clock::now(), is_eq(time_point(400_ms)));
        }

        end_test();
    }

    mutex& mtx_;
    T& cv_;
    bool& flag_;
};

template<typename T>
class low_prio_thread;
template<typename T>
using low_prio_thread_base = thread<low_prio_thread<T>>;
template<typename T>
class low_prio_thread : public low_prio_thread_base<T> {
    friend low_prio_thread_base<T>;

public:
    low_prio_thread(mutex& mtx, T& cv, bool& flag) : mtx_(mtx), cv_(cv), flag_(flag) {}

private:
    void thread_function() {
        { // 1. wait test
            this_thread::sleep_for(50_ms);
            cv_.notify_one();
        }

        { // 2. wait with predicate test
            this_thread::sleep_for(20_ms);
            cv_.notify_one();

            this_thread::sleep_for(30_ms);
            flag_ = true;
            cv_.notify_one();
        }

        { // 3. 4. wait_for timeout/no timeout test
            this_thread::sleep_for(50_ms);
            cv_.notify_one();
        }

        { // 5. 6. wait_for with predicate timeout with false/true predicate test
            this_thread::sleep_for(30_ms);
            flag_ = true;

            this_thread::sleep_for(10_ms);
        }

        { // 7. wait_for with predicate no timeout
            this_thread::sleep_for(30_ms);
            cv_.notify_one();

            this_thread::sleep_for(30_ms);
            flag_ = true;
            cv_.notify_one();
        }

        { // 8. 9. wait_for with predicate timeout with false/true predicate test
            this_thread::sleep_for(30_ms);
            flag_ = true;

            this_thread::sleep_for(10_ms);
        }

        { // 10. wait_for with predicate no timeout
            this_thread::sleep_for(30_ms);
            cv_.notify_one();

            this_thread::sleep_for(30_ms);
            flag_ = true;
            cv_.notify_one();
        }

        { // 11. wait test
            this_thread::sleep_for(50_ms);
            cv_.notify_all();
        }
    }

    mutex& mtx_;
    T& cv_;
    bool& flag_;
};

int main(int argv, char** argc) {
    start_test("condition_variable_test", argv, argc);

    static mutex mtx;
    static condition_variable cv;
    static bool flag{false};
    static high_prio_thread high(mtx, cv, flag);
    static low_prio_thread low(mtx, cv, flag);

    kernel::initialize(50_MHz, 100_Hz);

    high.start();
    low.start();

    kernel::start();
    return 0;
}
