#include "satos/tester.h"
#include "semihosting.h"
#include <span>
#include <ranges>
#include <cstdio>
#include <algorithm>
#include <fmt/format.h>

namespace satos_tester {
namespace detail {

tester& tester::instance() {
    static tester t;
    return t;
}

void tester::set_name(std::string_view name) { name_ = name; }

std::string_view tester::get_name() { return name_; }
void tester::start() { started_ = true; }
bool tester::is_started() { return started_; }
void tester::set_assert_failed() { assert_failed_ = true; }
bool tester::get_assert_failed() { return assert_failed_; }

} // namespace detail

void start_test(std::string_view name, int argc, char** argv) {
    auto args = std::span(argv, argc) |
                std::views::transform([](char const* c) { return std::string_view(c); });
    if (std::ranges::find(args, "--gtest_list_tests") != args.end()) {
        fmt::print("satos_test.\n");
        fmt::print("  {}\n", name);
        semihosting::exit(0);
    }
    detail::tester::instance().set_name(name);
    fmt::print("Starting satos_test.{}\n", name);
    detail::tester::instance().start();
}

void end_test() {
    auto name = detail::tester::instance().get_name();
    fmt::print("Finishing satos_test.{}\n", name);
    if (detail::tester::instance().get_assert_failed()) {
        semihosting::exit(1);
    }
    semihosting::exit(0);
}
} // namespace satos_tester