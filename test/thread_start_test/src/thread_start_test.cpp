#include "satos/kernel.h"
#include "satos/thread.h"
#include "satos/tester.h"
#include "satos/formatters/formatters.h"
#include "satext/units.h"

using namespace satos;
using namespace satos_tester;

class test_thread;
using test_thread_base = thread<test_thread, thread_priority::high_0>;
class test_thread : public test_thread_base {
    friend test_thread_base;

private:
    void thread_function() {
        { // test if thread is in running state
            assert_that(get_state(), is_eq(thread_state::running));
        }

        end_test();
    }
};

int main(int argv, char** argc) {
    start_test("thread_start_test", argv, argc);
    static test_thread t;

    { // test if thread is not started
        assert_that(t.get_state(), is_eq(thread_state::error));
        assert_that(t.native_handle(), is_eq(static_cast<void*>(nullptr)));
    }

    kernel::initialize(50_MHz, 100_Hz);

    t.start();

    { // test if thread is inactive and created with proper priority
        assert_that(t.get_state(), is_eq(thread_state::inactive));
        assert_that(t.native_handle(), is_not_eq(static_cast<void*>(nullptr)));
        assert_that(t.get_priority(), is_eq(thread_priority::high_0));
    }

    kernel::start();
    return 0;
}
