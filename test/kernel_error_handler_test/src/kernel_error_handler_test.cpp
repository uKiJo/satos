#include "satos/kernel.h"
#include "satos/tester.h"
#include "satos/formatters/formatters.h"
#include "satext/units.h"

using namespace satos;
using namespace satos_tester;

int main(int argv, char** argc) {
    start_test("kernel_error_handler", argv, argc);
    bool flag{false};

    kernel::set_error_handler([&flag](kernel::error e) {
        // check if error handler was properly called
        assert_that(e, is_eq(kernel::error::nullptr_dereference));
        flag = true;
    });

    kernel::error_handler(kernel::error::nullptr_dereference);

    // check if error handler was properly called
    assert_that(flag, is_eq(true));

    end_test();
    return 0;
}
