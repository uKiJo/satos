#include "satos/kernel.h"
#include "satos/thread.h"
#include "satos/timer.h"
#include "satos/tester.h"
#include "satext/units.h"
#include <array>

using namespace satos;
using namespace satos_tester;
using namespace satos::chrono_literals;

template<typename T>
class test_thread;

template<typename T>
using test_thread_base = thread<test_thread<T>, thread_priority::normal_0, 2048>;

template<typename T>
class test_thread : public test_thread_base<T> {
    friend test_thread_base<T>;

public:
    explicit test_thread(std::array<std::uint32_t, 3>& counters, T& timer) :
        counters_(counters), timer_(timer) {}

private:
    void thread_function() {
        { // test if no callbacks were called before timeout
            this_thread::sleep_for(50_ms);

            for (auto val : counters_) {
                assert_that(val, is_eq(0));
            }
        }

        { // test if all callbacks were called after timeout
            this_thread::sleep_for(60_ms);

            for (auto val : counters_) {
                assert_that(val, is_eq(1));
            }
        }

        end_test();
    }

    std::array<std::uint32_t, 3>& counters_;
    T& timer_;
};

int main(int argv, char** argc) {
    start_test("timer_mulitple_callbacks_test", argv, argc);

    static std::array<std::uint32_t, 3> counters{};
    static timer<3> t(100_ms);
    static test_thread test(counters, t);
    t.register_callback([]() { counters[0]++; });
    t.register_callback([]() { counters[1]++; });
    t.register_callback([]() { counters[2]++; });

    kernel::initialize(50_MHz, 100_Hz);

    t.start();
    test.start();

    kernel::start();
    return 0;
}
