#include "satos/kernel.h"
#include "satos/thread.h"
#include "satos/semaphore.h"
#include "satos/tester.h"
#include "satos/formatters/formatters.h"
#include "satext/units.h"

using namespace satos;
using namespace satos_tester;
using namespace satos::chrono_literals;
using time_point = satos::clock::time_point;

class high_prio_thread;
using high_prio_thread_base = thread<high_prio_thread, thread_priority::high_0>;
class high_prio_thread : public high_prio_thread_base {
    friend high_prio_thread_base;

public:
    high_prio_thread(binary_semaphore& taken, binary_semaphore& given) :
        taken_(taken), given_(given) {}

private:
    void thread_function() {
        { // 1. acquire already given semaphore
            given_.acquire();
            assert_that(clock::now(), is_eq(time_point(0_ms)));
        }

        { // 2. acquire taken semaphore
            taken_.acquire();
            assert_that(clock::now(), is_eq(time_point(50_ms)));
        }

        { // 3. try_acquire_until failure
            auto result = taken_.try_acquire_until(clock::now() + 20_ms);
            assert_that(result, is_eq(false));
            assert_that(clock::now(), is_eq(time_point(70_ms)));
        }

        { // 4. try_acquire_until success
            auto result = taken_.try_acquire_until(clock::now() + 40_ms);
            assert_that(result, is_eq(true));
            assert_that(clock::now(), is_eq(time_point(100_ms)));
        }

        { // 5. try_acquire_for failure
            auto result = taken_.try_acquire_for(20_ms);
            assert_that(result, is_eq(false));
            assert_that(clock::now(), is_eq(time_point(120_ms)));
        }

        { // 6. try_acquire_for success
            auto result = taken_.try_acquire_for(40_ms);
            assert_that(result, is_eq(true));
            assert_that(clock::now(), is_eq(time_point(150_ms)));
        }

        { // 7. try_acquire failure
            auto result = taken_.try_acquire();
            assert_that(result, is_eq(false));
        }

        { // 8. try_acquire success
            taken_.release();
            auto result = taken_.try_acquire();
            assert_that(result, is_eq(true));
        }

        end_test();
    }

    binary_semaphore& taken_;
    binary_semaphore& given_;
};

class low_prio_thread;
using low_prio_thread_base = thread<low_prio_thread>;
class low_prio_thread : public low_prio_thread_base {
    friend low_prio_thread_base;

public:
    low_prio_thread(binary_semaphore& taken, binary_semaphore& given) :
        taken_(taken), given_(given) {}

private:
    void thread_function() {
        { // 2. acquire taken semaphore
            this_thread::sleep_for(50_ms);
            taken_.release();
        }

        { // 3. 4. try_acquire_until failure/success
            this_thread::sleep_for(50_ms);
            taken_.release();
        }

        { // 5. 6. try_acquire_for failure/success
            this_thread::sleep_for(50_ms);
            taken_.release();
        }
    }

    binary_semaphore& taken_;
    binary_semaphore& given_;
};

int main(int argv, char** argc) {
    start_test("semaphore_test", argv, argc);

    static binary_semaphore taken;
    static binary_semaphore given(1);
    static high_prio_thread high(taken, given);
    static low_prio_thread low(taken, given);

    kernel::initialize(50_MHz, 100_Hz);

    high.start();
    low.start();

    kernel::start();
    return 0;
}
